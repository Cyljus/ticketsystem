
<!--<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>-->
<%@ page session='false' %><!-- verhindert das erstellen einer session wen jsp aufgerufen wird -->
<html>
	<head>
	<title>Auftrag Erstellen</title>
	<!--Bootstrap einbinden über CDN-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta charset="utf-8">
	<meta name="viewport" contend="width=device-width, initial-scale1">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row content">
				<!-- Navigationsleiste -->
				<div class="col-sm-2 sidenav">
					<h4>Auftr�ge</h4>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="Auftragspool">AuftragsPool</a></li>
						<li><a href="OffeneAuftrage">Offene Auftr�ge</a></li>
						<li> <a href="Auftrage">Angenommene Auftr�ge</a></li>
						<li class="active"> <a href="AuftragErstellen">Auftrag erstellen</a></li>
						
						<li> <a href="Logout">Logout</a></li>
					</ul>
				</div>
				<div class="col-sm-8">
					<div class="container-fluid">
						<!-- Auftrag erstellen formular  -->
						<form action="AuftragErstellenSubmit" method="Get">
							<div class="col-sm-4">
								<div class="form-group">
									<label for="Auftragstietel">Projekt Name:</label>
									<input type="text" class="form-control" id="Auftragstietel" name="Auftragstietel">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="Username">Zust�ndig</label>
									<select class="form-control" id="Username" name="Username">
									<option>Nicht Vergeben</option>
										<!-- Generiert beim aufruf User Liste -->
										<%=request.getAttribute("User")%>
									</select>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control" rows="6" id="Auftragszustand" name="Auftragszustand">Auftragszustand</textarea>
								</div>
							</div>
							<div class="col-sm-4">
								<button type="submit" class="btn btn-default">Senden</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>