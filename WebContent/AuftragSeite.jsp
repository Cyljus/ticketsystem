
<!--<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>-->
<%@ page session='false' %><!-- verhindert das erstellen einer session wen jsp aufgerufen wird -->
<html>
	<head>
	<title><%=request.getAttribute("AuftragsName")%></title>
	<!--Bootstrap einbinden �ber CDN-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta charset="utf-8">
	<meta name="viewport" contend="width=device-width, initial-scale1">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row content">
				<!-- Navigationsleiste -->
				<div class="col-sm-2 sidenav">
					<h4>Auftr�ge</h4>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="Auftragspool">AuftragsPool</a></li>
						<li> <a href="Auftrage">Angenommene Auftr�ge</a></li>
						<li> <a href="AuftragErstellen">Auftrag erstellen</a></li>
						<li> <a href="Logout">Logout</a></li>
					</ul>
				</div>
				<div class="col-sm-8">
					<div class="col-sm-4<!-- f�gt Auftragsname ein -->
					<div class="well well-sm">Auftrags Name: <%=request.getAttribute("AuftragsName")%></div>
					</div>
					<div class="col-sm-4"><!--f�gt Username ein -->
					<div class="well well-sm">Zust�ndiger Mitarbeiter: <%=request.getAttribute("Username")%></div>
					</div>
					<div class="col-sm-4"><!-- f�gt Auftragsstatus ein -->
						<div class="well well-sm">Bearbeitungs Status:<%=request.getAttribute("AuftragsStatus")%></div>
					</div>
					
						<!-- Auftrag �ndern formular  -->
					<form action="AuftragAndern" method="get">
					<div class="col-sm-12">
						<textarea class="form-control" rows="6" id="Auftragszustand" name="Auftragszustand"><%=request.getAttribute("AuftragsZustand")%></textarea>
						
						
					</div>
						<div class="col-sm-4">											<!-- giebt dem button die id des auftrags damit ich sie bei update des auftrags habe -->
						<button type="submit" class="btn btn-default" name="ID" value="<%=request.getAttribute("AuftragsID")%>"><%=request.getAttribute("BTNTXT")%></button>
					</div>																										<!-- f�gt text des buttons ein -->
					<div class="col-sm-4">
				
					</div>
					<div class="col-sm-4">
						<select class="form-control" name="Status">
							<option>Offen</option>
							<option>in Bearbeitung</option>
							<option>Erledigt</option>
						</select>
					</div>
					</form>
			</div>
		</div>
	</body>
</html>