<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session='false' %><!-- verhindert das erstellen einer session wen jsp aufgerufen wird -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
	<title>Auftragspool</title>
	<!--Bootstrap einbinden �ber CDN-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
	
	<meta name="viewport" contend="width=device-width, initial-scale1">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row content">
				<div class="col-sm-2 sidenav">
				<!--Linkes Hauptmen�-->
					<h4>Auftr�ge</h4>
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="Auftragspool">AuftragsPool</a></li>
						<li><a href="OffeneAuftrage">Offene Auftr�ge</a></li>
						<li> <a href="Auftrage">Angenommene Auftr�ge</a></li>
						<li> <a href="AuftragErstellen">Auftrag erstellen</a></li>

						<li> <a href="Logout">Logout</a></li>
					</ul>
				</div>
				<div class="col-sm-8">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Auftrag</th>
								<th>Mitarbeiter</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<!-- Erzeugt Liste aller Auftr�ge -->
							<%=request.getAttribute("Tabelle")%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>