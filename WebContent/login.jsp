<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page session='false' %><!-- verhindert das erstellen einer session wen jsp aufgerufen wird -->
<html>
	<head>
	<title>Login</title>
	<!--Bootstrap einbinden �ber CDN-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
	
	<meta name="viewport" contend="width=device-width, initial-scale1">
	</head>
	<body>
		<div class="container">
			
			<div class="row">
				 <div class="col-sm-4"> 
					<h3>Bitte Melden Sie Sich An</h3> 
				 </div>
			</div>
			<!-- Login Formular f�hrt zu Login.java -->
			<form action="Login" method="get">
				<div class="col-sm-4">
						<div class="form-group">
							<label for="name">Username:</label>
							<input type="text" class="form-control" id="name" name="name">
						</div>
						<div class="form-group">
							<label for="passwort">Passwort:</label>
							<input type="password" class="form-control" id="passwort" name="passwort">
						</div>
					<!-- Blendet altert ein wenn ein Alert an seite weitergegeben wird -->
					<%if(request.getAttribute("Alert") != null)out.println(request.getAttribute("Alert")); %>
					<button type="submit" class="btn btn-primary">Anmelden</button>
				 </div>
			</form>
		</div>
	</body>
</html>