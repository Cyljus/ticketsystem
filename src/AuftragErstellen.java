 

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AuftragErstellen")
public class AuftragErstellen extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public querry querry = new querry();
      
    public AuftragErstellen() {
        super();
    }
    
    // Wird vor AuftragErstellen.jsp aufgerufen und regelt dynamische elemente
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(false);
		//überprüft ob user eingeloggt ist
		if(session != null){
			try {
				//erzeugt die Auswahl liste mit usern denen man das projekt zuweisen kann
				request.setAttribute("User", querry.getUsers());
				
				RequestDispatcher rd = request.getRequestDispatcher("AuftragErstellen.jsp");
				rd.forward(request, response);
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}else{//Falls Nicht
			//Leitet auf Login um und blendet Alert ein
			RequestDispatcher rd =
					request.getRequestDispatcher("login.jsp");
			request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Bitte Melden sie sich an</strong></div>");
			rd.include(request, response);
		}
		out.close();
	}

}
