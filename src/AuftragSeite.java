

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AuftragSeite")
public class AuftragSeite extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public querry querry = new querry();
    
    public AuftragSeite() {
        super();
    }
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if(session != null){
			//nimmt auftragsid der anfrage entgegen
			String Auftragsid = request.getParameter("Auftragsid");
			try {
				//sendet querrry mit der id der anfrage
				ResultSet ergebnis = querry.getProject(Auftragsid);
					ergebnis.next();
					//speichert daten ziwschen
					String AuftragsID = ergebnis.getString(1);
					String AuftragsTietel = ergebnis.getString(2);
					String AuftragsZustand = ergebnis.getString(3);
					String AuftragsStatus = ergebnis.getString(4);
					String Zustandig = ergebnis.getString(5);
					//�bergibt daten an client
					request.setAttribute("AuftragsID", AuftragsID);
					request.setAttribute("AuftragsName", AuftragsTietel);
					request.setAttribute("AuftragsZustand", AuftragsZustand);
					request.setAttribute("AuftragsStatus", AuftragsStatus);
					request.setAttribute("Username", Zustandig);
					
					//dammit auf dem button nicht auftrag �bernemen will wen der user ihn bereits �bernommen hat
					if(Zustandig.equals((String)(session.getAttribute("User")))) {
						request.setAttribute("BTNTXT", "Speichern");
					}else{
						request.setAttribute("BTNTXT", "Auftrag �bernehmen");
					}
					//leite weiter Auf AuftragsSeite.jsp
					RequestDispatcher rd = request.getRequestDispatcher("AuftragSeite.jsp");
					rd.forward(request, response);
					
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			
		}else{//Falls Nicht
			//Leitet auf Login um und blendet Alert ein
			RequestDispatcher rd =
					request.getRequestDispatcher("login.jsp");
			request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Bitte Melden sie sich an</strong></div>");
			rd.include(request, response);
		}
	}
}
