

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AuftragErstellenSubmit")
public class AuftragErstellenSubmit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public querry querry = new querry();
       
    public AuftragErstellenSubmit() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false);
		//überprüft ob user angemeldet ist
		if(session != null){
			//nimmt die zum erstellen des auftrags erforderlichen parameter entgegen
			String Auftragstietel = request.getParameter("Auftragstietel");
			String Auftragszustand = request.getParameter("Auftragszustand");
			String Username = request.getParameter("Username");
			// sendet die daten an datenbank
			querry.addProject(Auftragstietel, Auftragszustand, Username);

			//Leitet auf Auftragserstellen weiter
			RequestDispatcher rd =
					request.getRequestDispatcher("AuftragErstellen");
			rd.include(request, response);
		
			}else{//Falls Nicht
			//Leitet auf Login um und blendet Alert ein
			RequestDispatcher rd =
					request.getRequestDispatcher("login.jsp");
			request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Bitte Melden sie sich an</strong></div>");
			rd.include(request, response);
		}
	}

}
	
