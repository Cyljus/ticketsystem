

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Auftragspool")
public class Auftragspool extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public querry querry = new querry();

    public Auftragspool() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(false);
		if(session != null){//�berpr�ft ob nutzer angemeldet ist
			try {//falls angemeldet
				//sendet SQL abfrage an datenbank und Generiert tabelle daraus
				request.setAttribute("Tabelle", querry.getProjects());
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			//leitet weiter auf Auftragspool.jsp
			RequestDispatcher rd = request.getRequestDispatcher("auftragspool.jsp");
			rd.forward(request, response);
			
		}else{// falls nicht
			//leitet auf login seite weiter
			//Blendet Alert ein
			RequestDispatcher rd =
					request.getRequestDispatcher("login.jsp");
			request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Bitte Melden sie sich an</strong></div>");
			rd.include(request, response);
		}
		out.close();
	}


}
