import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/AuftragAndern")
public class AuftragAndern extends HttpServlet{ 

	private static final long serialVersionUID = 1L;
	public querry querry = new querry();
	
    public AuftragAndern() {
        super();
        }

	//Wird aktiviert wen der auftrag von mitarbeiter angenommen oder bearbeitet wird
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		
		//überprüft ob user eingeloggt ist
		if(session != null){
			
			//Holt die geänderten parameter aus der Abfrage
			String ID = request.getParameter("ID");
			String AuftragsStatus = request.getParameter("Status");
			String Auftragszustand = request.getParameter("Auftragszustand");
			String  Zustandig = (String) session.getAttribute("User");
			//sendet update querry 
			
			querry.updateProject(ID, AuftragsStatus, Auftragszustand, Zustandig); 
			
			//Leitet auf Auftragspool weiter
			RequestDispatcher rd =
					request.getRequestDispatcher("Auftragspool");
			rd.include(request, response);
		}else{//Falls Nicht
				//Leitet auf Login um und blendet Alert ein
				RequestDispatcher rd =
						request.getRequestDispatcher("login.jsp");
				request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Bitte Melden sie sich an</strong></div>");
				rd.include(request, response);
		}
	}
}
