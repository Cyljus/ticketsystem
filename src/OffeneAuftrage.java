

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class OffeneAuftrage
 */
@WebServlet("/OffeneAuftrage")
public class OffeneAuftrage extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public querry querry = new querry();
    public OffeneAuftrage() {
        super();
    }
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			PrintWriter out = response.getWriter();
			HttpSession session = request.getSession(false);
			if(session != null){//�berpr�ft ob nutzer angemeldet ist
				//Fals nutzer angewendet
				try {
					//�bergibt Tabelle mit noch nicht Angefangenen Projekten
					request.setAttribute("Tabelle", querry.getOpenProjects());
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				
				//Leitet auf OffeneAuftrage.jsp weiter
				RequestDispatcher rd = request.getRequestDispatcher("OffeneAuftrage.jsp");
				rd.forward(request, response);
				}
			}else{//Falls Nicht
				//Leitet auf Login um und blendet Alert ein
				RequestDispatcher rd =
						request.getRequestDispatcher("login.jsp");
				request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Bitte Melden sie sich an</strong></div>");
				rd.include(request, response);
			}
			out.close();
		}
	   

}
