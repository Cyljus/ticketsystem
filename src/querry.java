import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
// Regelt Komunikation zwischen datenbanken und rest des programms
	public  class querry {
		
		
		//Nimmt Querry entgegen und Liefert ergebnis
		private ResultSet sendQuerry(String Querry) throws ClassNotFoundException, SQLException{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ticketsystem", "root", "");
			Statement Statement = con.createStatement();
			ResultSet Result = Statement.executeQuery(Querry);
			return Result;
		}
		private boolean sendInsert(String Querry) throws ClassNotFoundException, SQLException{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ticketsystem", "root", "");
			Statement Statement = con.createStatement();
			boolean Result = Statement.execute(Querry);
			return Result;
		}
		// Teste ob user "name" mit passwort "passwort" existiert und sende bool zur�ck
		public boolean userLogin(String name, String passwort) throws ClassNotFoundException, SQLException{
			ResultSet ergebnis = sendQuerry("SELECT * FROM `User` WHERE Username ='" + name + "' AND Passwort ='" + passwort + "'");
			return ergebnis.next();
		}
		//Generiert HTML Tabelle Allen auftr�gen sortiert nach Auftragsstatus
		public String getProjects() throws ClassNotFoundException, SQLException{
			String ergebnis = "";
			ResultSet result =  sendQuerry("Select Auftragstietel, Username , Auftragsstatus, AuftragsID from auftrag ORDER BY Auftragsstatus");
			 while(result.next()){
				 ergebnis = ergebnis + "<tr> <td> <a href=\"AuftragSeite?Auftragsid=" + result.getString(4) + "\">" + result.getString(1) + "</a> </td> <td> " + result.getString(2) + " </td> <td> " + result.getString(3) + "</td> </tr>";
			 }
			
			return ergebnis;
		}
		//Generiert HTML Tabelle Allen auftr�gen f�r die der angemeldete User zust�ndig ist sortiert nach Auftragsstatus
		public String getProjects(String user) throws ClassNotFoundException, SQLException{
			String ergebnis = "";
			ResultSet result =  sendQuerry("Select Auftragstietel, Auftragsstatus, AuftragsID from auftrag where Username ='" + user +"' ORDER BY Auftragsstatus");
			 while(result.next()){
				 ergebnis = ergebnis + "<tr> <td> <a href=\"AuftragSeite?Auftragsid=" + result.getString(3) + "\">" + result.getString(1) + " </td> <td> " + result.getString(2) + "</td> </tr>";
			 }
			 return ergebnis;
		}
			 public String getOpenProjects() throws ClassNotFoundException, SQLException{
					String ergebnis = "";
					ResultSet result =  sendQuerry("Select Auftragstietel, Auftragsstatus, AuftragsID from auftrag where Auftragsstatus ='Offen' ORDER BY Auftragsstatus");
					 while(result.next()){
						 ergebnis = ergebnis + "<tr> <td> <a href=\"AuftragSeite?Auftragsid=" + result.getString(3) + "\">" + result.getString(1) + " </td> ";
					 }
			 
			
			return ergebnis;
		}
		//Generiert optionsmen� mit allen nutzern zur auswahl
		public String getUsers() throws ClassNotFoundException, SQLException{
			String ergebnis = "";
			ResultSet result = sendQuerry("Select Username from User");
			 while(result.next()){
				 ergebnis = ergebnis + "<option> " + result.getString(1) + "</option> ";
			 }
			 return ergebnis;
		}
		
		//Gibt alle daten eines projekts aus
		public ResultSet getProject(String project) throws ClassNotFoundException, SQLException{
			return sendQuerry("Select * from auftrag where AuftragsID = '" + project + "'");
		}
		
		//Legt neues Projekt an
		public boolean addProject(String Auftragstietel,String Auftragszustand,String Username){
			boolean erfolgreich = false;
			try {
				erfolgreich = sendInsert("INSERT INTO `auftrag` (`AuftragsID`, `Auftragstietel`, `Auftragszustand`, `Auftragsstatus`, `Username`) "
						+ "VALUES (NULL, '" + Auftragstietel + "', '" + Auftragszustand + "', 'Offen', '" + Username + "')");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}	
				
				return erfolgreich;
		}
		//�ndert daten des projekts
		public boolean updateProject(String ID, String Auftragsstatus,String Auftragszustand,String Zust�ndig){
			boolean erfolgreich = false;
			try {
				erfolgreich = sendInsert("UPDATE `auftrag` SET `Auftragszustand` = '" + Auftragszustand + "', `Auftragsstatus` = '" + Auftragsstatus + "', `Username` = '" + Zust�ndig + "' WHERE `auftrag`.`AuftragsID` = " + ID);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}	
				
				return erfolgreich;
		}
		
		
	}