//Die Logik der Login Seite

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet("/Login")
public class Login extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    public querry querry = new querry();
    
    // Konstruktor
    public Login() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Die Parameter der anfrage �bergeben
		String name = request.getParameter("name");
		String passwort = request.getParameter("passwort");
		
		try {
			//sucht username name mit passwort in datenbank liefert true wenn entsprechender eintrag gefunden wurde
			if(querry.userLogin(name, passwort)){
				
					//Fals Login erfolgreich ist 
					HttpSession session = request.getSession();//startet http sitzung
					session.setAttribute("User", name); //f�gt nutzername der sitzung hinzu
					session.setMaxInactiveInterval(30*60);//Sitzung L�uft in 30 min ab
					
					//Leitet auf startseite weiter
					RequestDispatcher rd =
							request.getRequestDispatcher("Auftragspool");
					rd.forward(request, response);
				
			}else{//Falls Login nicht erfolgreich ist 
				//leitet auf Login seite weiter
				//Blendet Alert ein
				RequestDispatcher rd =
						request.getRequestDispatcher("login.jsp");
				request.setAttribute("Alert", "<div class=\"alert alert-danger\"><strong>Der benutzernamen und das passwort stimmen nicht ueberein</strong></div>");
				rd.include(request, response);
						
			}
		} // error handling
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
